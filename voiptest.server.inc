<?php

/**
 * @file
 * Implementations of the VoipServer with logging and configurable results.
 */

/**
 * Basic mock class.
 */
class VoipTestServer extends VoipServer {

  /**
   * Test server settings.
   *
   * Can be directly set if you have access to the server object; or can be set
   * as default using drupal variables - see the constructor.
   */

  /**
   * If this is an error server.
   *
   * @var bool
   */
  public $errorServer;

  /**
   * Error values.
   *
   * If errorServer is TRUE the error so set.
   *
   * Default:
   * array(
   *   'ErrorMessage' => 'Generic Error Text',
   *   'CallStatus' => VoipCall::ERROR,
   *   'HangupReason' => NULL, # not set
   * )
   *
   * Example for dial:
   * array(
   *   'ErrorMessage' => NULL, # not set
   *   'CallStatus' => VoipCall::BUSY,
   *   'HangupReason' => VoipCall::HANGUP_BUSY,
   * )
   *
   * @var array
   */
  public $errorValue;

  /**
   * Delay. How long should methods take.
   *
   * @var int
   */
  public $delay;

  /**
   * Constructor.
   */
  public function __construct() {
    $id = 'test';
    $name = t('Test server');
    parent::__construct($id, $name);

    $this->errorServer = variable_get('voiptest_server_errorserver', FALSE);
    $this->errorValue = array(
      'ErrorMessage' => 'Generic Error Text',
      'CallStatus' => VoipCall::ERROR,
      'HangupReason' => NULL,
    );
    $this->delay = variable_get('voiptest_server_delay', 0);
  }


  /*
   * API methods
   */

  function dial($voipcall, $server_config = NULL) {
    voip_error_reset();
    watchdog(
      'voip',
      'VoipTestServer dial() - cid:@cid - dest_number:@dest_number - server_config:@server_config',
      array(
        '@cid' => $voipcall->getCid(),
        '@dest_number' => $voipcall->getDestNumber(),
        '@server_config' => print_r($server_config, TRUE),
      ),
      WATCHDOG_INFO
    );
    sleep($this->delay);

    if ($this->errorServer) {
      if ($this->errorValue['ErrorMessage'] !== NULL) {
        voip_error($this->errorValue['ErrorMessage']);
        $voipcall->setErrorMessage($this->errorValue['ErrorMessage']);
      }
      $this->errorValue['CallStatus'] === NULL ? : $voipcall->setCallStatus($this->errorValue['CallStatus']);
      $this->errorValue['HangupReason'] === NULL ? : $voipcall->setHangupReason($this->errorValue['HangupReason']);
      $voipcall->save();
      return FALSE;
    }

    return TRUE;
  }

  function send_text($text, $voipcall, $reply = FALSE, $server_config = NULL) {
    voip_error_reset();
    watchdog(
      'voip',
      'VoipTestServer send_text() - text:@text - cid:@cid - dest_number:@dest_number - reply:@reply - server_config:@server_config',
      array(
        '@text' => $text,
        '@cid' => $voipcall->getCid(),
        '@dest_number' => $voipcall->getDestNumber(),
        '@reply' => print_r($reply, TRUE),
        '@server_config' => print_r($server_config, TRUE),
      ),
      WATCHDOG_INFO
    );
    sleep($this->delay);

    if ($this->errorServer) {
      $this->errorValue['ErrorMessage'] === NULL ? : $voipcall->setErrorMessage($this->errorValue['ErrorMessage']);
      $this->errorValue['CallStatus'] === NULL ? : $voipcall->setCallStatus($this->errorValue['CallStatus']);
      $this->errorValue['HangupReason'] === NULL ? : $voipcall->setHangupReason($this->errorValue['HangupReason']);
      $voipcall->save();
      return FALSE;
    }

    return TRUE;
  }

  function hangup($voipcall, $server_config = NULL) {
    watchdog(
      'voip',
      'VoipTestServer hangup() - cid:@cid - dest_number:@dest_number - server_config:@server_config',
      array(
        '@cid' => $voipcall->getCid(),
        '@dest_number' => $voipcall->getDestNumber(),
        '@server_config' => print_r($server_config, TRUE),
      ),
      WATCHDOG_INFO
    );
    sleep($this->delay);

    // @todo error server? Fail to hangup?

    return TRUE;
  }

  function text_handler($text_content, $origin, $destination, $time, $network, $server_name) {
    $params = array(
      'text_content' => $text_content,
      'origin' => $origin,
      'destination' => $destination,
      'time' => $time,
      'network' => $network,
      'server_name' => $server_name,
    );
    watchdog('voip', 'VoipTestServer text_handler() called with: @params', array('@params' => print_r($params, TRUE)), WATCHDOG_INFO);
    sleep($this->delay);
    return TRUE;
  }

  function ping($server_config = NULL) {
    $options = array('server_config' => $server_config);
    watchdog('voip', 'VoipTestServer ping() called with: @options', array('@options' => print_r($options, TRUE)), WATCHDOG_INFO);
    sleep($this->delay);
    return TRUE;
  }
}
